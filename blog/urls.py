from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView, ListView, CreateView

from blog import views
from . views import (
        comment_approve,
        comment_remove,
        PostListView,
        PostDetailView,
        CommentFormView
        )


urlpatterns = [
        url(r'^blog/$', PostListView.as_view(), name='list'),
        url(r'^(?P<slug>[\w-]+)/$', PostDetailView.as_view(), name='detail'),
        url(r'^form/$', CommentFormView.as_view(), name='form'),
        url(r'^comment/(?P<pk>\d+)/remove/$', views.comment_remove, name='comment_remove'),
        url(r'^comment/(?P<pk>\d+)/approve/$', views.comment_approve, name='comment_approve'),
        ]
