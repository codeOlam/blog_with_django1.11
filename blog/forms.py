from django import forms

from .models import Post, Comment


class CommentForm(forms.ModelForm):
    #comment_area = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Comment
        fields = ['name',
                'email',
                'text',]
