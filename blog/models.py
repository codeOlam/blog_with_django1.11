from django.db import models
from django.conf import settings
import datetime
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.utils import timezone
from django.core.urlresolvers import reverse

from .utils import unique_slug_generator

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    body = models.TextField()
    slug = models.SlugField(null=True, blank=True)
    publish = models.BooleanField(default=True)
    pub_date = models.DateTimeField('date_published')


    def __str__(self):
        return self.title


    def was_published_recently(self):
        now = timezone.now()


        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_discription = 'Latest Publish'


    def get_absolute_url(self):
        return reverse('blog:detail', kwargs={'slug':self.slug})

    def approved_comments(self):
        return self.comments.filter(approved_comment=True)


    class Meta:
        verbose_name = "Blog Entry"
        verbose_name_plural = "Blog Entries"
        ordering = ["-pub_date"]


class Comment(models.Model):
    post = models.ForeignKey('blog.Post', related_name='comments', null=True)
    name = models.CharField(max_length=150)
    email = models.EmailField(null=True, blank=True, default="Enter Valid Email Address")
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)


    def comments_approved(self):
        alert = "Approved"
        NotAlert = "Not Approved"
        if self.approved_comment == True:
            return alert
        else:
            return NotAlert
        self.save()

    def __str__(self):
        return self.name


    def get_abolute_url(self):
        return reverse('blog:detail', kwargs={'slug':self.name})


def entry_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


def entry_post_save_receiver(sender, instance, created, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)
        instance.save() 


pre_save.connect(entry_pre_save_receiver, sender=Post)
post_save.connect(entry_post_save_receiver, sender=Post)
