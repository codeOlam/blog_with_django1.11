from django.contrib import admin

from .models import Post, Comment

# Register your models here.


class PostAdmin(admin.ModelAdmin):
    fieldsets = [
            (None, {'fields': ['title', 'author', 'slug', 'body', 'publish']}),
            ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
            ]


    list_display = ('title', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['title', 'content']

class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'post', 'comments_approved')
 #   search_fields = ['post', 'name']  not working yet

admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
