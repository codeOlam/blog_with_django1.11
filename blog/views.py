from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView

from .models import Post, Comment
from .forms import CommentForm

# Create your views here.

def comment_approve(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve

    return redirect('blog/post_detail', pk=comment.post.pk)


def comment_remove(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.delete()

    return redirect('blog/post_detail', pk=comment.post.pk)

class PostListView(ListView):
    queryset = Post.objects.all()
    template_name = 'blog/post_list.html'


class PostDetailView(DetailView):
    queryset = Post.objects.all()
    form_class = CommentForm
    template_name = 'blog/post_detail.html'


    def post(self, request, slug, *args, **kwargs):
        form = CommentForm(request.POST)
        post = get_object_or_404(Post, slug=slug)

        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = self.get_object()
            #comment.post = post.get_object()
            comment.save()

            self.object = self.get_object()
            context = context = super().get_context_data(**kwargs)
            context['form'] = CommentForm

            return self.render_to_response(context=context)
            
        else:
            #form = CommentForm()
            self.object = self.get_object()
            context = super().get_context_data(**kwargs)
            context['form'] = form

            return self.render_to_response(context=context)

            
    def form_valid(self, form):
        comment = form.save(commit=False)
        comment.post = self.request.comments
        return super(PostDetailView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['form'] = CommentForm
        
        return context

    success_url = '/blog/'


class CommentFormView(FormView):
    #queryset = Comment.objects.all()
    form_class = CommentForm
    success_url = '/blog/'
